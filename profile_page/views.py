from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from templates.UserData import author, name, birthday, gender, email, description
from datetime import datetime, date
from .models import Profile, Expertise
from dashboard.views import response as from_dashboard

# Create your views here.

birthdate = birthday.strftime('%d %B')
exp = ['Breathing','Eating','Living']

def index(request):
        profile = Profile(name=name,gender=gender, description=description, email=email)
        expertise = Expertise(expertise=exp)
        response = {"style": from_dashboard["style"] ,'author': author, 'name' : name, 'expertise': expertise.expertise, 'birthday': birthdate,'gender':profile.gender, 'description':profile.description, 'email': profile.email}
        html = 'profile_page/profile_page.html'
        return render(request, html, response)
