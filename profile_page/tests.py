from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest
from .models import Profile

# Create your tests here.

class ProfilePageUnitTest(TestCase):

    def test_profile_page_url_is_exist(self):
        response = Client().get('/profile_page/')
        self.assertEqual(response.status_code,200)

    def test_profile_page_using_index_func(self):
        found = resolve('/profile_page/')
        self.assertEqual(found.func, index)

    def test_profile_page_add_content(self):
        profile = Profile.objects.create(name='hey',gender='Male', description='hallo', email='hi@gmail.com')
        counting_all_available_profile = Profile.objects.all().count()
        self.assertEqual(counting_all_available_profile,1)


