# Tugas 1 PPW | C-12
***

## Nama Anggota Kelompok
1. [Hanifa Arrumaisha] (https://gitlab.com/hanifaarrmsha/) (1606824332)
2. [Ibnu Sofian Firdaus] (https://gitlab.com/ibnufirdaus/) 
3. Nixi Sendya Putri (1606918383)
4. Syanindita Noor Intan Parameswari (1606875775)

## Link Herokuapp
[Web Kelompok C-12] (http://tugas-ppw-c12.herokuapp.com/)

***

#### Project Status
[![pipeline status](https://gitlab.com/ibnufirdaus/tugas-satu-c12/badges/master/pipeline.svg)](https://gitlab.com/ibnufirdaus/tugas-satu-c12/commits/master)

[![coverage report](https://gitlab.com/ibnufirdaus/tugas-satu-c12/badges/master/coverage.svg)](https://gitlab.com/ibnufirdaus/tugas-satu-c12/commits/master)

***


