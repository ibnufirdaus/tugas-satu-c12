from django.db import models

# Create your models here.
class Status(models.Model):
	status = models.TextField()
	created_date = models.DateTimeField(auto_now_add=True)

# class StatusExp(models.Model):
# 	status = models.TextField()
# 	created_date = models.DateTimeField(auto_now_add=True)
# 	slug = models.SlugField(unique=True)

# 	def get_absolute_url(self):
# 		return ('update_status',(), {'slug' :self.slug})

# 	def save(self, *args, **kwargs):
# 		if not self.slug:
# 			self.slug = slugify(self.title)
# 		super(StatusExp, self).save(*args, **kwargs)

class Comment(models.Model):
	name = models.TextField()
	comment = models.TextField()

# class CommentExp(models.Model):
# 	name = models.CharField(max_length=42)
# 	text = models.TextField()
# 	post = models.ForeignKey(Status)
# 	created_on = models.DateTimeField(auto_now_add=True)