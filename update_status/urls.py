from django.conf.urls import url
from .views import index, add_status, clear
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_status', add_status, name='add_status'),
    #url(r'^add_comment', comment, name='add_comment'),
    url(r'^clear/(?P<object_id>[0-9]+)', view=clear, name='clear')
]
