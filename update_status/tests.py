from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status, clear
from .models import Status
from .forms import Status_Form
class UpdateStatusUnitTest(TestCase):
	
	def test_update_status_url_is_exist(self):
		response = Client().get('/update_status/')
		self.assertEqual(response.status_code, 200)

	def test_update_status_using_index_func(self):
		found = resolve('/update_status/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_status(self):
		new_status = Status.objects.create(status='testing')
		counting_all_available_status = Status.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)

	def test_post_success_and_render_result(self):
		test = 'Testing 123'
		response_post = Client().post('/update_status/add_status',{'status':test})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('/update_status/')
		html_response = response.content.decode('utf8')
		self.assertIn(test,html_response)

	def test_post_error_and_render_result(self):
		test = 'Testing testing'
		response_post = Client().post('/update_status/add_status',{'status':''})
		self.assertEqual(response_post.status_code,302)

		response = Client().get('/update_status')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test,html_response)

	#Add Comment Test Case
	# def test_update_status_comment_url_is_exist(self):
	# 	response = Client().get('/update_status/add_comment/')
	# 	self.assertEqual(response.status_code,200)

	# def test_add_comment_using_comment_func(self):
	# 	found = resolve('/update_status/add_comment/')
	# 	self.assertEqual(found.func, comment)

	# def test_model_can_create_new_comment(self):
	# 	new_comment = Comment.objects.create(comment='testing')
	# 	counting_all_available_comment = Comment.objects.all().count()
	# 	self.assertEqual(counting_all_available_comment,1)

	# def test_add_comment_post_success_and_render_the_result(self):
	# 	comment = 'HaiHai'
	# 	response = Client().post('/update_status/add_comment', {'name': '', 'comment': comment})
	# 	self.assertEqual(response.status_code, 200)
	# 	html_response = response.content.decode('utf8')
	# 	self.assertIn(comment,html_response)

	# def test_comment_error_and_render_result(self):
	# 	test = 'Testing testing'
	# 	response_post = Client().post('/update_status/add_comment',{'name':'', 'comment':''})
	# 	self.assertEqual(response_post.status_code,302)
	# 	response = Client().get('/update_status/')
	# 	html_response = response.content.decode('utf8')
	# 	self.assertNotIn(test,html_response)

	def test_clear(self):
		new_status = Status.objects.create(status='Ini mau coba di delete')
		object_id = Status.objects.all().count()
		clear(Status,object_id)
		after_delete = Status.objects.all().count()
		self.assertEqual(after_delete,0)
