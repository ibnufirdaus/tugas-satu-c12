from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    description_attrs = {
        'type': 'text',
        'cols': 135,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':'What are you feeling today?'
    }

    status = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))

class Comment_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    attrs = {
        'class': 'form-control',
        'cols': 100,
        'rows': 2,
    }

    name = forms.CharField(label='Name', required=False, max_length=27, empty_value='Anonymous', widget=forms.Textarea(attrs=attrs))
    comment = forms.CharField(label='Comment', required=True, widget=forms.Textarea(attrs=attrs))

# class CommentForm(forms.ModelForm):
#     class Meta:
#         model = Comment
#         exclude = ['post']