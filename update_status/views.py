from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status
from templates.UserData import name,author
from dashboard.views import response as from_dashboard

response = {}

def index(request):
    response['name'] = name
    response['author'] = author
    status = Status.objects.all().order_by('-created_date')
    response['status'] = status
    html = 'update_status/update_status.html'
    response['status_form'] = Status_Form
    response["style"] = from_dashboard["style"]
    return render(request, html, response)


def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        status = Status(status=response['status'])
        status.save()
        return HttpResponseRedirect('/update_status/')
    else:
        return HttpResponseRedirect('/update_status/')

response = {}
# def comment(request):
#     comment = Comment.objects.all()
#     response['comment'] = comment
#     html = 'update_status/add_comment.html'
#     response['comment_form'] = Comment_Form
#     return render(request, html, response)

# def add_comment(request):
#     form = Comment_Form(request.POST or None)
#     if(request.method == 'POST' and form.is_valid()):
#         response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
#         response['comment'] = request.POST['comment']
#         comment = Comment(name=response['name'], comment=response['comment'])
#         comment.save()
#         html ='update_status/add_comment.html'
#         return render(request, html, response)
#     else:        
#         return HttpResponseRedirect('/update_status/')

def clear(request,object_id):
    clearing = Status.objects.get(pk=object_id)
    clearing.delete()
    return HttpResponseRedirect('/update_status/')

# def view_post(request, slug):
#     post = get_object_or_404(StatusExp, slug=slug)
#     form = CommentForm(request.POST or None)
#     if form.is_valid():
#         comment = form.save(commit=False)
#         comment.post = post
#         comment.save()
#         return redirect(request.path)
#     return render_to_response('update_status/add_comment.html',
#                               {
#                                   'post': post,
#                                   'form': form,
#                               },
#                               context_instance=RequestContext(request))
