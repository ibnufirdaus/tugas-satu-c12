from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Friend
from .forms import Friend_Form

# Create your tests here.
class addFriendUnitTest(TestCase):
    def test_add_friend_url_is_exist(self):
        response = Client().get('/add_friend/')
        self.assertEqual(response.status_code, 200)

    def test_add_friend_using_index_func(self):
        found = resolve('/add_friend/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_friend(self):
        new_friend = Friend.objects.create(name='Syanindita', URL='http://tugas-ppw-c12.herokuapp.com')
        counting_all_available_friend=Friend.objects.all().count()
        self.assertEqual(counting_all_available_friend,1)

    def test_form_friend_input_has_placeholder(self):
        form = Friend_Form()
        self.assertIn('placeholder="Isikan Nama Temanmu"', form.as_p())
        self.assertIn('placeholder="Isikan URL Temanmu"', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Friend_Form(data={'name':'', 'URL':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )

    def test_str_equal_to_message(self):
        cobaFriend = Friend.objects.create(name='Hanifa', URL='http://tugas-ppw-c12.herokuapp.com')
        self.assertEqual(cobaFriend.__str__(), cobaFriend.name)

    def test_add_friend_post_fail(self):
        dump = {'name':'', 'URL':''}
        response = self.client.get('/add_friend/add_friend/', dump)
        self.assertEqual(response.status_code, 302)

    def test_add_friend_get_success(self):
        response = self.client.post('/add_friend/add_friend/', {'name': 'Ibnu', 'URL': 'http://tugas-ppw-c12.herokuapp.com'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/add_friend')
        new_response = self.client.get('/add_friend/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Ibnu', html_response)

    def test_add_friend_and_render(self):
        cobaFriend = Friend.objects.create(name='Hanifa', URL='http://tugas-ppw-c12.herokuapp.com')
        response = self.client.get('/add_friend/')
        html_response = response.content.decode('utf8')
        self.assertIn(cobaFriend.name, html_response)
        self.assertIn(cobaFriend.URL, html_response)
