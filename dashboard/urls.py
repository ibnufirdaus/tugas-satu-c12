from django.conf.urls import url
from .views import index, changeStyle

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'changeStyle', changeStyle, name="changeStyle")
]
