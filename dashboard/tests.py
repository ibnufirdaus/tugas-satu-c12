from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index , changeStyle, response as from_dashboard
from update_status.models import Status
# Create your tests here.

class DashboardUnitTest(TestCase):
    def test_dashboard_url_is_exist(self):
        response = Client().get('/dashboard/')
        self.assertEqual(response.status_code, 200)

    def test_dashboard_using_index_func(self):
        found = resolve('/dashboard/')
        self.assertEqual(found.func, index)

    def test_if_feeds_is_zero(self):
        test = "This user has not posted anything"
        response = Client().get('/dashboard/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_if_feeds_is__not_zero(self):
        test = "This user has not posted anything"
        new_status = Status.objects.create(status='testing')
        response = Client().get('/dashboard/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test,html_response)

    def test_if_style_can_be_changed(self):
        found = resolve('/dashboard/changeStyle')
        self.assertEqual(found.func, changeStyle)
        changeStyle(Status)
        self.assertEqual(0,from_dashboard["style"])