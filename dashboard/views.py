from django.shortcuts import render
from update_status.models import Status
from add_friend.models import Friend
from templates.UserData import name, author
from datetime import *
from django.http import HttpResponseRedirect


response = {"style": 1}

def index(request):
    feedsAmount = Status.objects.count()
    if feedsAmount > 0:
        latestFeed = Status.objects.all()[feedsAmount-1]
        message = latestFeed.status
        time = latestFeed.created_date
    else:
        message = "This user has not posted anything"
        time = date(datetime.now().year,datetime.now().month,datetime.now().day)

    friendAmount = Friend.objects.count()

    response['author'] =  author
    response['name'] =  name
    response['friendsAmount'] =  friendAmount
    response['feedsAmount'] =  feedsAmount
    response['latestFeed'] =  message
    response['created'] =  time

    return render(request, 'dashboard_page.html', response)

def changeStyle(request):
    response["style"] = abs(response["style"]-1)
    return HttpResponseRedirect('/dashboard/')
